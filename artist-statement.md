# Artist Statement

Through my colour picker, I am trying to show that simplicity can lead to complexity and difficulty.

The goal of my colour picker is simple, move the pointer so it gets through the slots to the desired colour.

I took inspiration from one of the most frustrating arcade games I played as a kid - the sega key Master. The premise was simple, insert the pointer through a hole to push down the prize. At first glance it seems easy, but through this simplicity, players are surprised when they fail at a seemingly easy task.

Inspired by contemporary mobile app design, I had decided to depict this colour picker with a very simple design , consisting of whole colours and simple shapes to further show that simplicity can lead to difficulty.

I have used random jittering and also shortened the slot holes to enhance difficulty in this game.



How to Play:
Space = Shoot
W = up
S = down

(cheat):
E = change colour options
