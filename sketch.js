//Septian Razi
//u6086829
//comp1720 - Assignment2


// this is the variable your picker should change
var pickedColour = {colour : [255,0,0,255],
									}

//the options displayed in my colour
var colourOptions = [
											[0,0,0],
											[255,0,0],
											[0,255,0],
											[0,0,255],
											[100,200,0],
											[74,65,42,255],
										];


//variables for the size of canvas
var canWidth = 800;
var canHeight = 800;
var colHeight = canHeight/5;

var value = 10000; // value used for the picker slider y position

//variables used to control game states
var inGame = true;
var inTransition = !inGame;
var lose = false;
var level = 0;
var difficultySlot = 19 - (4*level);
var jitter = level;

//other various variables acccesed by multiple functions
var pickerWidth = 50;
var pickerHeight = 20;
var picked = false;
var gotThrough = false;
var s;
var slotHoleHeight = pickerHeight +difficultySlot ;
var opacity = 0;



function setup() {
    createCanvas(canWidth, canHeight);
		noStroke();
}

function draw() {

		background(0,100,sin(frameCount/70)*255,50);
		drawBackground();
		noStroke()
    // display the currently "picked" colour in the bottom-right

    fill(pickedColour.colour);
		rect(-20,-20, 30,canHeight+50,7);
		rect(-20, value % canHeight, 20+ pickerWidth,pickerHeight,7);
		console.log("picked " + picked);
		console.log("slotHoleHeight " + slotHoleHeight);
		fill(0);
		rect(10,-20, 10,canHeight+50,7);
		drawColourOptions();
		if (reverseTransitionStage){
			transOpacity = transOpacity -20;
			if (transOpacity <= 0) {
				reverseTransitionStage = false;
			}
		}
		if (!picked){
			pickerWidth = 50;
			inGame = true;
			inTransition = !inGame;
			if (keyIsDown(87)) {
				value = value - 3;
			} else if (keyIsDown(83)) {
				value= value + 3;
			}
				value = value + random(-jitter,jitter);
		} else {
				inGame = false;
				inTransition = !inGame;

				if (gotThrough) {
					if (pickerWidth < canWidth-w){
						pickerWidth= pickerWidth + 16;
					} else {
						transition(checkWhichColour(value%canHeight), value%canHeight, true);
						if (millis() > s+2500){
							fill(0,0,0,opacity-255*6);
							printRGB(pickedColour.colour);
							opacity = opacity + 5;
						}
					}

				} else {
					lose = true;
					if (pickerWidth < canWidth-w-45 ){
						pickerWidth= pickerWidth + 10;
					} else {
						transition(colourOptions[5], value%canHeight, false);
						if (lose && millis() > s+2500) {
							textSize(35);
							fill(0,0,0,opacity-255);
							text("You chose opaque couché,",30,canHeight/2);
							text("the worlds ugliest colour :(",30,40 + canHeight/2);
							printRGB(colourOptions[5]);
							opacity = opacity + 5;
						}
						}
					}
				}
}

// functions used when player presses key
function keyReleased() {
	console.log("a key has been pressed");
  if (keyCode === 32 && inGame) {
		console.log("space has been pressed");
    picked = true;
		gotThrough =	checkIfThrough(value%canHeight);
		opacity = 0;
		transOpacity = 255;
		s = millis();
  } else if (keyCode === 32 && inTransition && !lose) {
		console.log("inTransitionSpace");
		changeColourOptions();
		picked = false;

		fillPickerLength = 1;
		fillPickerStart = canWidth;
		grow = 0;
		setAlready = false
		transOpacity = 255;
		jitter = level;

		reverseTransitionStage = true;

		level++
		difficultySlot = 19 - (3*level);
		slotHoleHeight = pickerHeight +difficultySlot;

		value = 255*30 + randomGaussian(0,255) ;
		s = millis();

	} else if (keyCode === 69) {
		console.log("test");
		changeColourOptions();
		console.log(pickedColour.colour);

	}
  return false; // prevent any default behavior
}


//function called to drawBackground
function drawBackground(){

	fill(255,255,255,100)
	var x;
	var y;
	if (frameCount % 30 < 15){
		for (i = 0; i<1; i++){
			x = random(0,canWidth);
			y = random(0,canHeight);
			r = randomGaussian(30,30);
			ellipse(x,y,r,r);
		}
	}

}

//function to print the rgb of the chosen colour
function printRGB (colour){

		textSize(35);
		fill(0,0,0,opacity-255*2.5);
		text("R : "+colour[0],30,100 + canHeight/2);
		fill(0,0,0,opacity-255*4);
		text("G : "+colour[1],30,140 + canHeight/2);
		fill(0,0,0,opacity-255*5.5);
		text("B : "+colour[2],30,180 + canHeight/2);

}

var colours;
h = colHeight;
w = canWidth/5;

//function used to change colour options in the choosing screen
function changeColourOptions() {
		var r = pickedColour.colour[0];
		var g = pickedColour.colour[1];
		var b = pickedColour.colour[2];

		for (i=0; i<5; i++){
			rN = ceilingfloorColour(round(randomGaussian(r,150)));
			gN = ceilingfloorColour(round(randomGaussian(g,150)));
			bN = ceilingfloorColour(round(randomGaussian(b,150)));

			colourOptions[i] = [rN,gN,bN];

		}
}

// function to round up to 255 or 0
function ceilingfloorColour (n){
	if (n<0){
		return 0;
	} else if (n>255){
		return 255;
	} else{
		return n;
	}
}
//function to draw the colour options
function drawColourOptions(){

	colours = Object.values(colourOptions);
	for (i = 0; i<5; i++){
		fill(colours[i]);
		rect(canWidth-w, (i*h), w,h);
	}
	drawSlots();
}

var openingXs = []; // array to record the opening slots x values

//function to draw the slots and rectangle in front of colours
function drawSlots(){
	openingXs = [];
	fill(1 + sin(frameCount/50)*255);
	var slotWidth = 20;
	var slotRectsHeight = (colHeight - slotHoleHeight)/2;

	var slotRange = [];
	for (i = 0; i < 6; i++){
		slotRange = [];
		slotRange.push(colHeight*i+ slotRectsHeight);
		slotRange.push(colHeight*i + slotRectsHeight+slotHoleHeight);
		openingXs.push(slotRange);
		rect(canWidth-w-50,  -10+colHeight*i, slotWidth, 10+slotRectsHeight,7);
		rect(canWidth-w-50, (colHeight*i) + slotRectsHeight+slotHoleHeight, slotWidth, slotRectsHeight,7);
	}
}

//function to check if user gets through slot or not
function checkIfThrough(value) {
	console.log("checkIfThrough " + value);
	var res = false;
	for (i = 0; i<openingXs.length; i++){
		var top= checkIfOneThrough(value,openingXs[i]);
		var bottom =checkIfOneThrough(value+pickerHeight,openingXs[i]);
		var through = top && bottom;
		if (through) {
			return true;
			break
		}
	}
	return res;
}

function checkIfOneThrough(x, [x1,x2]){
	if (x >= x1 && x <= x2){
		return true;
	} else {
		return false;
	}
}

//function to see which colour the user chose
function checkWhichColour(value){
	if (value<colHeight){
		return colourOptions[0];
	} else if (value<colHeight*2){
			return colourOptions[1];
	} else if (value<colHeight*3){
			return colourOptions[2];
	} else if (value<colHeight*4){
			return colourOptions[3];
	} else {
			return colourOptions[4];
	}
}

//variables used in transition
var reverseTransitionStage = false;
var fillPickerLength = 1;
var fillPickerStart = canWidth;
var grow = 0;
var setAlready = false
var transOpacity = 255;

//function to do the transition from picking colour to showing rgb
function transition(colour,value,notHit){

	if (!notHit && !setAlready){
		fillPickerStart = canWidth - w - 40;
		setAlready = true;
	}

	if (millis() > s+1500){
		pickedColour.colour  = colour;
		fill(colour[0],colour[1], colour[2], transOpacity);
		rect(0,value, canWidth, grow);
		rect(0,value, canWidth, -grow);
		grow = grow + 20;

	} else if (millis() > s+300){
		fillPicker(colour, value);
		fillPickerLength = fillPickerLength +20;
		fillPickerStart = fillPickerStart -20;
	}

}
function fillPicker(colour, value){
	fill(colour);
	rect(fillPickerStart,value,fillPickerLength,pickerHeight,7);
}

//Septian Razi
//u6086829
//comp1720 - Assignment2
