# Statement of Originality

I declare that everything I have submitted in this assignment is entirely my own
work, with the following exceptions:

## Inspiration

- The inspiration of using a semi opaque background came from Ben's second praxis on starry night

## code
- the code used for the keypressed function was taken from the p5 reference on keyPressed()
